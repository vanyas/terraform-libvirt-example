resource "libvirt_network" "kube_network" {
  name = "kube"
  mode = "nat"
  domain = var.domain_name
  addresses = var.network_cidr
  dns {
    enabled = true
  }
}
