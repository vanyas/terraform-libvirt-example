data "template_file" "inventory" {
  template = file("${path.module}/inventory.tpl")
  vars = {
    workers = join("\n",formatlist("%s ansible_host=%s",
                   libvirt_domain.workers.*.network_interface.0.hostname,flatten(libvirt_domain.workers.*.network_interface.0.addresses)))
    controllers = join("\n",formatlist("%s ansible_host=%s",
               libvirt_domain.controllers.*.network_interface.0.hostname,flatten(libvirt_domain.controllers.*.network_interface.0.addresses)))
  }
}

resource "local_file" "ansible_inventory" {
  content = data.template_file.inventory.rendered
  file_permission = 0644
  filename = "ansible/inventory"
}

resource "null_resource" "ansible" {
  triggers = {
    inventory = data.template_file.inventory.rendered
    playbook_sha1 = sha1(file("ansible/playbook.yml"))
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i inventory playbook.yml"
    working_dir = "ansible"
    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
}
