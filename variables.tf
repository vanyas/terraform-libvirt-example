variable "LIBVIRT_HOST" {
}

variable "controllers" {
  default = {
    count = 3
    vcpu = 2
    memory = 4096
    disk_size = 21474836480
    address = "10.240.0.1%s"
    mac = "52:de:ad:be:ef:1%s"
  }
}

variable "workers" {
  default = {
    count = 3
    vcpu = 2
    memory = 4096
    disk_size = 53687091200
    address = "10.240.0.2%s"
    mac = "52:de:ad:be:ef:2%s"
  }
}

variable "network_cidr" {
  default = ["10.240.0.0/24"]
}

variable "domain_name" {
  default = "kube.loc"
}

variable "user" {
  default = {
    name = "sid"
    passwd = "$6$WtYgB1PR$lf8mjgly6gx9RPZahZR1/qugSql5FhIrWTz4GPAK7O/1JV2QpjzRLsEerN8L3i6awyYEgj4JqCuRQ3Ob/yvrC/"
    ssh_authorized_keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDqDDaaDf3JuwePnkJc0MDx9SevAR6oUOCHmR2WcSwNWkLhAOMan3XSOTqUJAo3/CCHrhlpguM2jSgctryUGxCt5iZkwUreB2pFHNNH3U1FfHgK3KIqqoHCtTbbGuppRGFeJJvvXeXJHB87iBFixIfrZg9jJYABLN73mnmw6JKMukV0MvCLaj3o1XqT0G6mKBu8e+qPE3hJ5ukKlyuY4bF6wkCsqQ0dShE9GiYY0ALCc5fjgK1EBpzl/JXSP3AusknjVmzm70oshQKh/TCiAWO9zvwrEtIxf4gBjQ3TmZRBNbp0iloR5Ib/Vc6xRAKq3M17suF+Lwwogh2EGaL5LQah sid@semenov-laptop"
  }
}
