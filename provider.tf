provider "libvirt" {
    uri = "qemu+ssh://${var.LIBVIRT_HOST}/system?socket=/var/run/libvirt/libvirt-sock"
}
