resource "libvirt_domain" "controllers" {
  count  = var.controllers.count
  
  name   = "controller-${count.index}"
  memory = var.controllers.memory
  vcpu   = var.controllers.vcpu

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
   network_id     = libvirt_network.kube_network.id
   hostname       = "controller-${count.index}"
   addresses      = [format(var.controllers.address,count.index)]
   mac            = format(var.controllers.mac,count.index) 
   wait_for_lease = true
  }

  disk {
    volume_id = element(libvirt_volume.disk_controller.*.id, count.index)
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }
  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }
  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}


resource "libvirt_domain" "workers" {
  count  = var.workers.count

  name   = "worker-${count.index}"
  memory = var.workers.memory
  vcpu   = var.workers.vcpu

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_id     = libvirt_network.kube_network.id
    hostname       = "worker-${count.index}"
    addresses      = [format(var.workers.address,count.index)]
    mac            = format(var.workers.mac,count.index)
    wait_for_lease = true
  }

  disk {
   volume_id = element(libvirt_volume.disk_worker.*.id, count.index)
  }

  console {
   type        = "pty"
   target_port = "0"
   target_type = "serial"
  }
  console {
   type        = "pty"
   target_type = "virtio"
   target_port = "1"
  }

  graphics {
   type        = "spice"
   listen_type = "address"
   autoport    = true
  }
}


