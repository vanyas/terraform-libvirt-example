resource "libvirt_pool" "kube_hard" {
  name = "kube_hard"
  type = "dir"
  path = "/var/lib/libvirt/pool-kube-hard"
}

resource "libvirt_volume" "ubuntu_bionic" {
  name   = "bionic-server-cloudimg-amd64.img"
  pool   = libvirt_pool.kube_hard.name
  source = "https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_volume" "disk_controller" {
  count = var.controllers.count

  name           = "controller-${count.index}.qcow2"
  base_volume_id = libvirt_volume.ubuntu_bionic.id
  pool           = libvirt_pool.kube_hard.name
  size           = var.controllers.disk_size 
}

resource "libvirt_volume" "disk_worker" {
  count = var.workers.count

  name           = "worker-${count.index}.qcow2"
  base_volume_id = libvirt_volume.ubuntu_bionic.id
  pool           = libvirt_pool.kube_hard.name
  size           = var.workers.disk_size
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
  vars = {
    name = var.user.name
    passwd = var.user.passwd
    ssh_authorized_keys = var.user.ssh_authorized_keys
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config.cfg")
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "commoninit.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = libvirt_pool.kube_hard.name
}
